import React from 'react';

class FancySelectOption extends React.Component {
  render() {
    const className = this.props.selectedOption === this.props.value ? "option selected" : "option";
    return <span className={className} data-value={this.props.value} onClick={this.props.onOptionClick(this.props.value)}>{this.props.label}</span>;
  }
}

export default FancySelectOption;