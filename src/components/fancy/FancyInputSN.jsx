import React from 'react';

class FancyInputSN extends React.Component {
  render() {
    return (
    <div className="fancy-input fancy-item">
        <label className="fancy">S/N</label>
        <input className="fancy" name="sn" id="sn" type="text" autoComplete="off" data-value={this.props.value} value={this.props.value} onChange={this.props.onChange} />
    </div>
    );
  }
}

export default FancyInputSN;