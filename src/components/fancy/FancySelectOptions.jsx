import React from 'react';

import FancySelectOption from '../../components/fancy/FancySelectOption';
class FancySelectOptions extends React.Component {
  render() {
    const options = this.props.options.map((option) =>
      <FancySelectOption key={option.value} value={option.value} label={option.label} onOptionClick={this.props.onOptionClick} selectedOption={this.props.selectedOption} />
    );
    let addItem = '';
    if (this.props.addItem) {
      addItem = <div className="add-item allways white in-option">
        <i className="icon-add-circle"></i>
        <span>Add Tag</span>
      </div>;
    }
    return <div className="options">
      {addItem}
      {options}
    </div>;
  };
}

export default FancySelectOptions;
