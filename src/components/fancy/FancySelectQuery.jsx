import React from 'react';

class FancySelectQuery extends React.Component {
  constructor(props) {
    super(props);
    this.queryInput = React.createRef();
  }

  render() {
    if (this.props.focus) {
      this.queryInput.current.focus();
    }
    return <input ref={this.queryInput} type="text" placeholder="" name="query" value={this.props.queryText} className="query" onChange={this.props.onChange} autoComplete="off"></input>
  }
}

export default FancySelectQuery;