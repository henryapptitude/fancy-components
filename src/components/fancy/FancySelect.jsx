import React from 'react';
import FancySelectQuery from '../../components/fancy/FancySelectQuery';
import FancySelectOpener from '../../components/fancy/FancySelectOpener';
import FancySelectOptions from '../../components/fancy/FancySelectOptions';
import FancySelectRemoveQuery from '../../components/fancy/FancySelectRemoveQuery';

import '../../components/fancy/css/fancy-elements.scss';

class FancySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      removeQuery: false,
      open: false,
      queryText: '',
      fieldValue: '',
      selectedOption: '',
      visibleOptions: props.options
    };

    this.onClick = this.onClick.bind(this);
    this.onNewQuery = this.onNewQuery.bind(this);
    this.onOpenerClick = this.onOpenerClick.bind(this);
    this.onRemoveClick = this.onRemoveClick.bind(this);
    this.onOptionClick = this.onOptionClick.bind(this);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  };

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  render() {
    let className = "carbox-select fancy-item";
    if (this.state.open) className += " opened"
    if (this.state.selected) className += " selected";
    if (this.props.grey) className += " grey";
    if (this.props.double) className += " double";

    return <div ref={this.setWrapperRef} className={className} onClick={this.onClick}>
      <input type="hidden" className="hidden-val" name={this.props.fieldName} value={this.state.fieldValue}></input>
      <span className="label">{this.props.label}</span>
      <FancySelectQuery focus={this.state.open} onChange={this.onNewQuery} queryText={this.state.queryText} />
      <FancySelectOptions options={this.state.visibleOptions} onOptionClick={this.onOptionClick} selectedOptionKey={this.state.selectedOption} addItem={this.props.addItem} />
      <FancySelectOpener open={this.state.open} onClick={this.onOpenerClick} />
      <FancySelectRemoveQuery active={this.state.removeQuery} onClick={this.onRemoveClick} />
    </div>;
  };

  onOpenerClick(event) {
    event.stopPropagation();
    this.setState(state => ({
      open: !state.open,
      removeQuery: !state.removeQuery,
    }));
  };

  onRemoveClick(event) {
    event.stopPropagation();
    this.setState({
      removeQuery: false,
      open: false,
      selectedOption: '',
      queryText: '',
      fieldValue: '',
      visibleOptions: this.props.options
    });
  };


  onOptionClick(key) {
    return (event) => {
      const option = this.findOption(key);
      event.stopPropagation();
      this.setState({
        removeQuery: true,
        open: false,
        selectedOption: key,
        queryText: option.label,
        fieldValue: option.value
      });
    };
  };

  onClick(event) {
    event.stopPropagation();
    this.setState({
      removeQuery: true,
      open: true
    });
  }

  onNewQuery(event) {
    event.stopPropagation();
    if (event.keyCode === 8) { // backspace
      let visibleOptions;
      if (event.currentTarget.value === '') {
        visibleOptions = this.props.options;
      } else {
        visibleOptions = this.filterOptions(event.currentTarget.value);
      }
      this.setState({
        removeQuery: true,
        open: true,
        selectedOption: '',
        fieldValue: '',
        queryText: event.currentTarget.value,
        visibleOptions: visibleOptions
      });
      // showOptionsDropdown(select);
    } else {
      this.setState({
        queryText: event.currentTarget.value,
        visibleOptions: this.filterOptions(event.currentTarget.value)
      });
    }
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState(state => ({
        open: false,
        removeQuery: state.queryText.length > 0,
      }));
    }
  }

  findOption(key) {
    return this.props.options.find((option) => option.value === key);
  }

  filterOptions(filter) {
    const lowerFilter = filter.toLowerCase();
    return this.props.options.filter((option) => option.label.toLowerCase().includes(lowerFilter));
  }
}

export default FancySelect;