import React from 'react';

class FancySelectRemoveQuery extends React.Component {
  render() {
    const className = this.props.active ? 'remove v' : 'remove';
    return <span className={className} onClick={this.props.onClick}><i className="icon-close-small"></i></span>
  }
}

export default FancySelectRemoveQuery;