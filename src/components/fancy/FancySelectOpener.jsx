import React from 'react';

class FancySelectOpener extends React.Component {
  render() {
    const className = this.props.open ? "open opened" : "open";
    return <span className={className} onClick={this.props.onClick}><i className="icon-chevron-more"></i></span>
  };
}

export default FancySelectOpener;