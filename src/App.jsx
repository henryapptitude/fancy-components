import React from 'react';
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import FancySelect from './components/fancy/FancySelect'
import FancyInputSN from './components/fancy/FancyInputSN'

import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
  state = {
    options: {
      maker: [
        { label: "AC Cars", value: "27", maker: "27" },
        { label: "Alfa Romeo", value: "3" },
        { label: "Allard Cadillac", value: "23" },
        { label: "Aston Martin", value: "7" },
        { label: "ATS", value: "20" },
        { label: "Benetton", value: "38" }
      ],
      model: [
        { label: "Cobra 289", value: "1", maker: "27" },
        { label: "12C/316", value: "2", maker: "3" },
        { label: "1750", value: "3", maker: "3" },
        { label: "1750 GTV", value: "4", maker: "3" },
        { label: "DB2", value: "5", maker: "7" },
        { label: "DB2/4", value: "6", maker: "7" },
        { label: "DB3", value: "7", maker: "7" },
        { label: "DB3S", value: "8", maker: "7" }
      ]
    }
  };

  render() {
    return (
      <div className="">
        <h2>Accordion</h2>
        <Accordion>
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="0">
                Tab One
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
              <Card.Body>Hello! I'm the body of the Tab One</Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Card.Header>
              <Accordion.Toggle as={Button} variant="link" eventKey="1">
                Tab Two
              </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
              <Card.Body>Hello! I'm another body,  of the Tab Two</Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>

        <h2>FancySelect</h2>
        <FancySelect key="fancy-select-a1" addItem={true} options={this.state.options.maker} label="MAKER" fieldName="maker" />

        <h2>FancySelect multiple</h2>
        <div className="attached-items mt30">
          <FancySelect key="fancy-select-b1" addItem={true} options={this.state.options.maker} label="MAKER" fieldName="maker" />
          <FancySelect key="fancy-select-b2" addItem={true} options={this.state.options.model} label="MODEL" fieldName="model" />
          <FancyInputSN key="fancy-select-b3" value="" onChange={() => {}} />
        </div>
      </div>
    );
  }
}

export default App;
